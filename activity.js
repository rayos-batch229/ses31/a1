/*

1. What built-in JS keyword is used to import packages?

2. What Node.js module/package contains a method for server creation?

3. What is the method of the http object responsible for creating a server using Node.js?

4. Between server and client, which creates a request?

5. Between server and client, which creates a response?

6. What is a runtime environment used to create stand-alone backend applications written in Javascript?

7.What is the largest registry for Node packages?

*/

/*

ANSWERS:

1. require()

2.http module

3.createServer()

4.Client

5.Server

6.NodeJS

7.NPM - Node Package Manager

*/ 